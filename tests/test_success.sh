#!/bin/bash

# shellcheck disable=SC1073
for f in tests/success/*; do
  echo $f;
  java -jar out/artifacts/Progetto_jar/Progetto.jar -i $f
done