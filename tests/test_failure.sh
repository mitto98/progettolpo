#!/bin/bash

# shellcheck disable=SC1073
for f in tests/failure/*; do
  echo "$f";
  head -n 1 "$f"
  java -jar out/artifacts/Progetto_jar/Progetto.jar -i "$f"
  echo "";
done