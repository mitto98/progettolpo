#!/bin/bash

# shellcheck disable=SC1073
for f in tests/failure/no-type-check/*; do
  echo "$f";
  head -n 1 "$f"
  java -jar out/artifacts/Progetto_jar/Progetto.jar -ntc -i "$f"
  echo "";
done