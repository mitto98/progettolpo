package lab11_05_06;

import lab11_05_06.parser.*;
import lab11_05_06.parser.StreamTokenizer;
import lab11_05_06.parser.ast.Prog;
import lab11_05_06.visitors.evaluation.Eval;
import lab11_05_06.visitors.evaluation.EvaluatorException;
import lab11_05_06.visitors.typechecking.TypeCheck;
import lab11_05_06.visitors.typechecking.TypecheckerException;


import java.io.*;

import static java.lang.System.err;
import static java.lang.System.exit;


//Simple Shell for L++
public class Main {
    private static void printHelp() {
        System.out.println("-i filename:  spacifica il file con il sorgente, se omesso viene usato stdin");
        System.out.println("-o filename:  specifica il file di output, se omesso viene usato stdout");
        System.out.println("-ntc:         Il type-checking non viene eseguito (no-type-checking)");
        System.out.println("--help:       Show this help message :D");
    }

    private static void runIterpreter(InputStreamReader in, PrintWriter out, boolean typeChecking) {
        try (Tokenizer tokenizer = new StreamTokenizer(in)) {
            Parser parser = new MyParser(tokenizer);
            Prog prog = parser.parseProg();
            if (typeChecking) {
                prog.accept(new TypeCheck());
            }
            prog.accept(new Eval(out));
        } catch (TokenizerException e) {
            err.println("Tokenizer error: " + e.getMessage());
        } catch (ParserException e) {
            err.println("Syntax error: " + e.getMessage());
        } catch (TypecheckerException e) {
            err.println("Static error: " + e.getMessage());
        } catch (EvaluatorException e) {
            err.println("Dynamic error: " + e.getMessage());
        } catch (Throwable e) {
            err.println("Unexpected error.");
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    public static void main(String[] args) {
        String fileIn = null, fileOut = null;
        boolean tc = true;
        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                switch (args[i]) {
                    case "-i":
                        if (++i >= args.length) {
                            err.println("No input file specified!");
                            exit(2);
                        }
                        fileIn = args[i];
                        break;
                    case "-o":
                        if (++i >= args.length) {
                            err.println("No output file specified!");
                            exit(2);
                        }
                        fileOut = args[i];
                        break;
                    case "-ntc":
                        tc = false;
                        break;
                    case "--help":
                        printHelp();
                        exit(0);
                    default:
                        err.println("invalid option " + args[i]);
                        err.println("try --help for more information.");
                        exit(1);
                }
            }
        }

        InputStreamReader in = null;
        PrintWriter out = null;
        try {
            //In
            if (fileIn == null) {
                in = new InputStreamReader(System.in);
            } else {
                in = new FileReader(fileIn);
            }
            //Out
            if (fileOut == null)
                out = new PrintWriter(System.out, true);
            else {
                out = new PrintWriter(fileOut);
            }

            runIterpreter(in, out, tc);
        } catch (FileNotFoundException e) {
            err.println("No such file: " + e.getMessage());
        } catch (Throwable e) {
            err.println("Unexpected error.");
            e.printStackTrace();
        } finally {
            try {
                if (in != null) in.close();
                if (out != null) out.close();
            } catch (IOException e) {
                err.println("Unexpected error.");
                e.printStackTrace();
            }
        }
    }
}
