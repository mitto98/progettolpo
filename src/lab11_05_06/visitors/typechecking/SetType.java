package lab11_05_06.visitors.typechecking;

import static java.util.Objects.requireNonNull;

public class SetType implements Type {
    private final Type setType;
    public static final String TYPE_NAME = "SET";

    public SetType(Type setType) {
        this.setType = requireNonNull(setType);
    }

    public Type getSetElementsType() {
        return setType;
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof SetType))
            return false;
        SetType st = (SetType) obj;
        return setType.equals(st.setType);
    }

    @Override
    public int hashCode() {
        return setType.hashCode();
    }

    @Override
    public String toString() {
        return setType+" "+TYPE_NAME;
    }
}
