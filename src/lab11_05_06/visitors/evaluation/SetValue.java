package lab11_05_06.visitors.evaluation;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
public class SetValue implements Value, Iterable {
    private HashSet<Value> set;

    private SetValue(HashSet<Value> values) {
        set = values;
    }

    public SetValue() {
        this.set = new HashSet<Value>();
    }

    public SetValue(Value o) {
        this();
        add(o);
    }

    public boolean add(Value o) {
        return set.add(o);
    }

    @Override
    public Iterator iterator() {
        return set.iterator();
    }

    @Override
    public SetValue asSet() {
        return this;
    }

    @Override
    public String toString() {
        StringBuilder ret = new StringBuilder("{");
        Iterator it = iterator();
        while (it.hasNext()) {
            ret.append(it.next());
            if (it.hasNext()) ret.append(",");
        }
        return ret.append("}").toString();
    }

    @Override
    public int hashCode() {
        return set.hashCode();
    }

    @Override
    public final boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof SetValue)) {
            return false;
        }
        SetValue oset = (SetValue) obj;
        if(set.size() != oset.set.size())
            return false;
        for (Object o : oset) {
            if (!set.contains((Value) o))
                return false;
        }
        return true;
    }

    public IntValue size() {
        return new IntValue(set.size());
    }


    public SetValue intersect(SetValue other) {
        HashSet<Value> intersection = new HashSet<>(set);
        intersection.retainAll(other.set);
        return new SetValue(intersection);
    }

    public SetValue union(SetValue other) {
        HashSet<Value> union = new HashSet<>(set);
        union.addAll(other.set);
        return new SetValue(union);
    }

    public BoolValue contains(Value o) {
        return new BoolValue(set.contains(o));
    }
}
