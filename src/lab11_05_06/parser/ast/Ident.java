package lab11_05_06.parser.ast;

public interface Ident extends Exp {
	String getName();
}
