package lab11_05_06.parser.ast;

import lab11_05_06.visitors.Visitor;

public class WhileStmt implements Stmt {
    private final Exp exp;
    private final Block iterBlock;

    public WhileStmt(Exp exp, Block iterBlock) {
        this.exp = exp;
        this.iterBlock = iterBlock;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + exp + "," + iterBlock + ")";
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitWhileStmt(exp, iterBlock);
    }
}
