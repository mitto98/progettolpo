package lab11_05_06.parser.ast;

import lab11_05_06.visitors.Visitor;

public class HexLiteral extends PrimLiteral<Integer> {

    public HexLiteral(int n) {
        super(n);
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitIntLiteral(value);
    }
}
