package lab11_05_06.parser.ast;

import lab11_05_06.visitors.Visitor;

public class SingleExp extends Single<Exp> implements ExpSeq {

    public SingleExp(Exp single) {
        super(single);
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visiSingleExp(single);
    }
}
