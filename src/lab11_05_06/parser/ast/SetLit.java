package lab11_05_06.parser.ast;

import static java.util.Objects.requireNonNull;

import lab11_05_06.visitors.Visitor;

import java.util.HashSet;

public class SetLit implements Exp {
    private ExpSeq expSeq;

    public SetLit(ExpSeq expSeq) {
        this.expSeq = requireNonNull(expSeq);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+"("+expSeq+")";
    }

    @Override
    public <T> T accept(Visitor<T> visitor) {
        return visitor.visitSetLit(expSeq);
    }
}
